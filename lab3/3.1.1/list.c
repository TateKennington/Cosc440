/* **************** LDD:1.0 s_07/lab1_list.c **************** */
/*
 * The code herein is: Copyright Jerry Cooperstein, 2009
 *
 * This Copyright is retained for the purpose of protecting free
 * redistribution of source.
 *
 *     URL:    http://www.coopj.com
 *     email:  coop@coopj.com
 *
 * The primary maintainer for this code is Jerry Cooperstein
 * The CONTRIBUTORS file (distributed with this
 * file) lists those known to have contributed to the source.
 *
 * This code is distributed under Version 2 of the GNU General Public
 * License, which you should have received with the source.
 *
 */
/* Linked lists
 *
 * Write a module that sets up a doubly-linked circular list of data
 * structures.  The data structure can be as simple as an integer
 * variable.
 *
 * Test inserting and deleting elements in the list.
 *
 * Walk through the list (using list_entry()) and print out values to
 * make sure the insertion and deletion processes are working.
 @*/

#include <linux/module.h>
#include <asm/atomic.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/init.h>

static LIST_HEAD(my_list);

struct my_entry {
	struct list_head clist;	
	int val;
};

void print_list(void){
	struct my_entry *curr;

	list_for_each_entry(curr, &my_list, clist){
		printk(KERN_INFO "val = %d\n", curr->val);
	}
}

static int __init my_init(void)
{
	struct my_entry *ce;
	int k;
	for(k=0; k<5; k++){
		ce = (struct my_entry*)kmalloc(sizeof(struct my_entry), GFP_KERNEL);
		ce->val = k;
		INIT_LIST_HEAD(&ce->clist);
		list_add_tail(&ce->clist, &my_list);
	}
	printk(KERN_INFO "List Initialized\n");

	print_list();

	return 0;
}



static void __exit my_exit(void)
{
	struct list_head *list;	/* pointer to list head object */
	struct list_head *tmp;	/* temporary list head for safe deletion */
	
	list_for_each_safe(list, tmp, &my_list){
		list_del(list);
		kfree(list_entry(list, struct my_entry, clist));
	}

	if(list_empty(&my_list)){
		printk(KERN_INFO "Successfully deleted list\n");
	}
	else{
		printk(KERN_INFO "Failed to delete list, remaining:\n");
		print_list();
	}
}

module_init(my_init);
module_exit(my_exit);

MODULE_AUTHOR("Dave Harris");
/* many modifications by Jerry Cooperstein */
MODULE_DESCRIPTION("LDD:1.0 s_07/lab1_list.c");
MODULE_LICENSE("GPL v2");
