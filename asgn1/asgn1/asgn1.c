
/**
 * File: asgn1.c
 * Date: 22/08/2018
 * Author: Tate Kennington 
 * Version: 0.1
 *
 * This is a module which serves as a virtual ramdisk which disk size is
 * limited by the amount of memory available and serves as the requirement for
 * COSC440 assignment 1 in 2012.
 *
 * Note: multiple devices and concurrent modules are not supported in this
 *       version.
 */
 
/* This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version
 * 2 of the License, or (at your option) any later version.
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/list.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/mm.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/device.h>
#include <linux/sched.h>

#define MYDEV_NAME "asgn1"
#define MYIOC_TYPE 'k'

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Tate Kennington");
MODULE_DESCRIPTION("COSC440 asgn1");


/**
 * The node structure for the memory page linked list.
 */ 
typedef struct page_node_rec {
  struct list_head list;
  struct page *page;
} page_node;

typedef struct asgn1_dev_t {
  dev_t dev;            /* the device */
  struct cdev *cdev;
  struct list_head mem_list; 
  int num_pages;        /* number of memory pages this module currently holds */
  size_t data_size;     /* total data size in this module */
  atomic_t nprocs;      /* number of processes accessing this device */ 
  atomic_t max_nprocs;  /* max number of processes accessing this device */
  struct kmem_cache *cache;      /* cache memory */
  struct class *class;     /* the udev class */
  struct device *device;   /* the udev device node */
} asgn1_dev;

asgn1_dev asgn1_device;


int asgn1_major = 0;                      /* major number of module */  
int asgn1_minor = 0;                      /* minor number of module */
int asgn1_dev_count = 1;                  /* number of devices */


/**
 * This function frees all memory pages held by the module.
 */
void free_memory_pages(void) {
  page_node *curr;
  page_node *tmp;
  
  //Iterate through every page node
  list_for_each_entry_safe(curr, tmp, &(asgn1_device.mem_list), list){
	
	if(curr->page){ //The node has a page allocated
	
		//Free the allocated page
		printk(KERN_WARNING "Attempting to free page: %ld\n", 
		       page_to_pfn(curr->page));
		__free_page(curr->page);
	}

	//Free the page node
	printk(KERN_INFO "Attempting to free page node\n");
	list_del(&(curr->list));
	kmem_cache_free(asgn1_device.cache, curr);
  }

  //Shrink the cache
  kmem_cache_shrink(asgn1_device.cache);

  //Reset the device metadata
  asgn1_device.num_pages = 0;
  asgn1_device.data_size = 0;

}


/**
 * This function opens the virtual disk, if it is opened in the write-only
 * mode, all memory pages will be freed.
 */
int asgn1_open(struct inode *inode, struct file *filp) {
  
   //Increase the process count
   atomic_inc(&asgn1_device.nprocs);

   if(atomic_read(&asgn1_device.nprocs)>atomic_read(&asgn1_device.max_nprocs)){ //We have reached the max number of processes
	//Reject the device, and decrement the process count
   	printk(KERN_INFO "Rejected open, maximum processes exceeded\n");
	atomic_dec(&asgn1_device.nprocs);
   	return -EBUSY;
   }

   if(filp->f_mode & FMODE_WRITE && !(filp->f_mode & FMODE_READ)){ //Device is opened as write-only
	printk(KERN_INFO "Opened as write-only, freeing memory\n");
   	free_memory_pages();
   }
	
   printk(KERN_INFO "Process opened device\n");

   return 0; /* success */
}


/**
 * This function releases the virtual disk, but nothing needs to be done
 * in this case. 
 */
int asgn1_release (struct inode *inode, struct file *filp) {
  
  printk(KERN_INFO "Process released device\n");

  //Decrease the process count
  atomic_dec(&asgn1_device.nprocs);
	
  return 0;
}


/**
 * This function reads contents of the virtual disk and writes to the user 
 */
ssize_t asgn1_read(struct file *filp, char __user *buf, size_t count,
		 loff_t *f_pos) {
  size_t size_read = 0;                          /* size read from virtual disk in this function */
  size_t begin_offset = *f_pos % PAGE_SIZE;      /* the offset from the beginning of a page to
			                            start reading */
  int begin_page_no = *f_pos / PAGE_SIZE;        /* the first page which contains
					            the requested data */
  int curr_page_no = 0;                          /* the current page number */
  size_t curr_size_read;                         /* size read from the virtual disk in this round */
  size_t size_to_be_read;                        /* size to be read in the current round in 
			                            while loop */

  //struct list_head *ptr = asgn1_device.mem_list.next;
  page_node *curr;

  //Log that the read function was entered
  printk(KERN_INFO "Process attempting read. Page no:%d Offset:%d\n",begin_page_no, begin_offset);

  if(*f_pos > asgn1_device.data_size){ //The process is trying to read past EOF
	printk(KERN_INFO "Tried to read past EOF\n");
	return 0;
  }

  if(asgn1_device.mem_list.next == &(asgn1_device.mem_list)){ //The device is empty
  	printk(KERN_INFO "Memory list empty\n");
	return 0;
  }

  if(*f_pos + count>asgn1_device.data_size){ //The process is trying to read more than is available

  	//Cap the read to the end of memory
  	count = asgn1_device.data_size - *f_pos;
  }

  //Iterate through every page in memory
  curr = list_entry(asgn1_device.mem_list.next, page_node, list);
  while(curr_page_no < begin_page_no){ //While he haven't reached the first page

  
	if(curr->list.next == &(asgn1_device.mem_list)){ //We've reached the end of the memory list
		
		//Log what happened and return EOF
		printk(KERN_INFO "Traversal reached end of list\n");
		return 0;
	}

	//Move to the next page and increment the count
	curr = list_entry(curr->list.next, page_node, list);
	curr_page_no++;
  }

  //While we havent read the requested amount
  while(size_read < count){

	//Set size_to_be_read to the remaining amount or the entire page, whichever is smaller
	size_to_be_read = min(count-size_read, PAGE_SIZE-begin_offset);

	//Copy size_to_be_read bytes to the user
	curr_size_read = 0;
	while(curr_size_read < size_to_be_read){ //We haven't finished copying from this page

		//Try to copy the remaining amount of bytes to the user
		int size_not_read = copy_to_user(buf, page_address(curr->page)+begin_offset+curr_size_read, size_to_be_read - curr_size_read);

		//Increment curr_size_read and shift the buffer pointer to after what was copied
		buf += size_to_be_read-curr_size_read-size_not_read;
		curr_size_read += size_to_be_read-curr_size_read-size_not_read;
	}

	//Increment size_read
	size_read += curr_size_read;

	//If we've reached the end of the memory list break
	if(curr->list.next == &(asgn1_device.mem_list)){
		printk(KERN_INFO "Read reached end of list\n");
		break;
	}

	//Move onto the next page
	curr = list_entry(curr->list.next, page_node, list);
	begin_offset = 0;
  }

  //Update the position in the file and return the number of bytes read
  *f_pos+=size_read;
  return size_read;
}




static loff_t asgn1_lseek (struct file *file, loff_t offset, int cmd)
{
    loff_t testpos = 0; //Temporary value for the new file position
    size_t buffer_size = asgn1_device.num_pages * PAGE_SIZE; //Total size of the device currently

    printk(KERN_INFO "Process attempting seek");

    //Check the command type
    switch(cmd){
	//Set the position relative to the beginning
	case SEEK_SET:
		testpos = offset;
		break;
	//Set the position relative to the current position
	case SEEK_CUR:
		testpos = file->f_pos + offset;
		break;
	//Set the position relative to the end of the buffer
	case SEEK_END:
		testpos = buffer_size + offset;
		break;
    }

    //Clamp the offset between 0 and buffer_size
    if(testpos<0) testpos = 0;
    if(testpos>buffer_size) testpos = buffer_size;

    //Update the file position
    file->f_pos = testpos;

    //Log and return the new file position
    printk (KERN_INFO "Seeking to pos=%ld\n", (long)testpos);
    return testpos;
}


/**
 * This function writes from the user buffer to the virtual disk of this
 * module
 */
ssize_t asgn1_write(struct file *filp, const char __user *buf, size_t count,
		  loff_t *f_pos) {
  size_t orig_f_pos = *f_pos;                    /* the original file position */
  size_t size_written = 0;                       /* size written to virtual disk in this function */
  size_t begin_offset = *f_pos % PAGE_SIZE;      /* the offset from the beginning of a page to
			                            start writing */
  int begin_page_no = *f_pos / PAGE_SIZE;        /* the first page this finction
					            should start writing to */

  int curr_page_no = 0;     /* the current page number */
  size_t curr_size_written; /* size written to virtual disk in this round */
  size_t size_to_be_written;  /* size to be read in the current round in 
				 while loop */
  
  //struct list_head *ptr = asgn1_device.mem_list.next;
  page_node *curr;

  printk(KERN_INFO "Process attempting write\n");

  //If the memory list is empty add a new page
  if(asgn1_device.mem_list.next == &(asgn1_device.mem_list)){
	page_node* new;
	new = kmem_cache_alloc(asgn1_device.cache, GFP_ATOMIC);
	if(new == NULL){
		printk(KERN_WARNING "Not enough memory left\n");
		return -ENOMEM;
	}

	new->page = alloc_page(GFP_KERNEL);
	if(new->page == NULL){
		printk(KERN_WARNING "Not enough memory left\n");
		return -ENOMEM;
	}

	list_add_tail(&(new->list), &(asgn1_device.mem_list));
	asgn1_device.num_pages++;
	printk(KERN_INFO "Page list empty, adding page\n");
  }

  //Set the current page to the first one
  curr = list_entry(asgn1_device.mem_list.next, page_node, list);

  //While we havent reached the first page
  while(curr_page_no < begin_page_no){
	
	//If we reach the end of the page list add a new page
	if(curr->list.next == &(asgn1_device.mem_list)){
		page_node* new;
		new = kmem_cache_alloc(asgn1_device.cache, GFP_ATOMIC);
		if(new == NULL){
			printk(KERN_WARNING "Not enough memory left\n");
			return -ENOMEM;
		}

		new->page = alloc_page(GFP_KERNEL);
		if(new->page == NULL){
			printk(KERN_WARNING "Not enough memory left\n");
			return -ENOMEM;
		}

		list_add_tail(&(new->list), &(asgn1_device.mem_list));
		asgn1_device.num_pages++;
		printk(KERN_INFO "Traversal reached end of page list, adding page\n");	
	}

	//Move to the next page and increment the count
	curr = list_entry(curr->list.next, page_node, list);
	curr_page_no++;
  }
  
  //While we havent written the requested amount
  while(size_written < count){

	//Set size_to_be_written to the remaining amount to be written or to the size of the page, whichever is smaller
	size_to_be_written = min(count-size_written, PAGE_SIZE-begin_offset);

	//Write size_to_be_written bytes to the user buffer
	curr_size_written = 0;
	while(curr_size_written < size_to_be_written){
		int size_not_written = copy_from_user(page_address(curr->page)+begin_offset+curr_size_written, buf, size_to_be_written-curr_size_written);

		//Update curr_size_written and move the user buf pointer to the end of what has been written
		buf += size_to_be_written-curr_size_written-size_not_written;
		curr_size_written += size_to_be_written - curr_size_written - size_not_written;
	}

	//Update the total size written
	size_written += curr_size_written;

	//If we've reached the end of the page list and another page
	if(curr->list.next == &(asgn1_device.mem_list)){
		page_node* new;
		new = kmem_cache_alloc(asgn1_device.cache, GFP_ATOMIC);
		if(new == NULL){
			printk(KERN_WARNING "Not enough memory left\n");
			return -ENOMEM;
		}

		new->page = alloc_page(GFP_KERNEL);
		if(new->page == NULL){
			printk(KERN_WARNING "Not enough memory left\n");
			return -ENOMEM;
		}

		list_add_tail(&(new->list), &(asgn1_device.mem_list));
		asgn1_device.num_pages++;
		printk(KERN_INFO "Write reached end of page list, adding page\n");	
	}

	//Move to the next page
	curr = list_entry(curr->list.next, page_node, list);
	begin_offset = 0;
  }
  
  //Update the file position and the total data_size, then return the amount written
  *f_pos += size_written;
  asgn1_device.data_size = max(asgn1_device.data_size,
                               orig_f_pos + size_written);
  return size_written;
}

#define SET_NPROC_OP 1
#define TEM_SET_NPROC _IOW(MYIOC_TYPE, SET_NPROC_OP, int) 

/**
 * The ioctl function, which nothing needs to be done in this case.
 */
long asgn1_ioctl (struct file *filp, unsigned cmd, unsigned long arg) {
  int new_nprocs;
  int result;

  if(_IOC_TYPE(cmd) != MYIOC_TYPE){ //The commands magic number is wrong
	printk(KERN_INFO "Invalid ioctl command, %d\n", cmd);
	return -EINVAL;
  }

  //Check the command type
  switch(cmd){
	
	//The command is to set the max number of processes
	case TEM_SET_NPROC:

		//Log the attempt
		printk(KERN_INFO "Process attempting to set max processes\n");

		//Copy the arguments from user space and check for errors
		result = copy_from_user(&new_nprocs, (void*)arg, sizeof(int));
		if(result>0){
			printk(KERN_WARNING "Failed to copy argument from user space\n");
			return -EAGAIN;
		}

		if(new_nprocs>=1){ //The argument is atleast one
			printk("Set max number of processes to %d\n", new_nprocs);
			atomic_set(&(asgn1_device.max_nprocs), new_nprocs);
			return 0;
		}

		//Print that an error occured and exit
		printk(KERN_INFO "Invalid argument\n");
		return -EINVAL;

	//We dont recognize the command	
	default:
		return -EINVAL;

  }
  return -ENOTTY;
}


static int asgn1_mmap (struct file *filp, struct vm_area_struct *vma)
{
    unsigned long offset = vma->vm_pgoff << PAGE_SHIFT;
    unsigned long len = vma->vm_end - vma->vm_start;
    unsigned long ramdisk_size = asgn1_device.num_pages * PAGE_SIZE;
    page_node *curr;
    unsigned long index = 0;

    printk(KERN_INFO "Process attempting mmap\n");

    //Check that the offset and length are valid
    if(offset<0 || offset+len>ramdisk_size || len<0){
	printk("mmap region invalid\n");
	return -EINVAL;
    }

    //Iterate through each memory page
    list_for_each_entry(curr, &(asgn1_device.mem_list), list){

	//If we have mapped the required length
    	if(PAGE_SIZE*index>=offset+len){
    		printk(KERN_INFO "Mmap successful\n"); 
		return 0;
	}

	//If the current page is in the mapped region
	if(index>=offset){

		//Log and map the page appropriately
		remap_pfn_range(vma, vma->vm_start + PAGE_SIZE*index, page_to_pfn(curr->page), 1, vma->vm_page_prot);
	}
	index++;
    }
   
    printk(KERN_INFO "Mmap successful\n"); 
    return 0;
}


struct file_operations asgn1_fops = {
  .owner = THIS_MODULE,
  .read = asgn1_read,
  .write = asgn1_write,
  .unlocked_ioctl = asgn1_ioctl,
  .open = asgn1_open,
  .mmap = asgn1_mmap,
  .release = asgn1_release,
  .llseek = asgn1_lseek
};


static void *my_seq_start(struct seq_file *s, loff_t *pos)
{
if(*pos >= 1) return NULL;
else return &asgn1_dev_count + *pos;
}
static void *my_seq_next(struct seq_file *s, void *v, loff_t *pos)
{
(*pos)++;
if(*pos >= 1) return NULL;
else return &asgn1_dev_count + *pos;
}
static void my_seq_stop(struct seq_file *s, void *v)
{
/* There's nothing to do here! */
}

int my_seq_show(struct seq_file *s, void *v) {
  
  //Display module information
  seq_printf(s, "Pages: %d\nData Size: %d\nCurrent Processes: %d\nMax Processes: %d\n", 
		  asgn1_device.num_pages, asgn1_device.data_size, 
		  atomic_read(&asgn1_device.nprocs), atomic_read(&asgn1_device.max_nprocs));
  return 0;


}


static struct seq_operations my_seq_ops = {
.start = my_seq_start,
.next = my_seq_next,
.stop = my_seq_stop,
.show = my_seq_show
};

static int my_proc_open(struct inode *inode, struct file *filp)
{
    return seq_open(filp, &my_seq_ops);
}

struct file_operations asgn1_proc_ops = {
.owner = THIS_MODULE,
.open = my_proc_open,
.llseek = seq_lseek,
.read = seq_read,
.release = seq_release,
};




/**
 * Initialise the module and create the master device
 */
int __init asgn1_init_module(void){
  int result; 
  asgn1_device.dev = MKDEV(asgn1_major, asgn1_minor);

  //Create proc entry and report any errors
  if(proc_create("asgn1", 0, NULL, &asgn1_proc_ops) == NULL){
	printk(KERN_WARNING "Failed to create proc entry\n");
  }

  //Initialize the device members
  atomic_set(&(asgn1_device.nprocs), 0);
  atomic_set(&(asgn1_device.max_nprocs), 1);
  asgn1_device.num_pages = 0;
  asgn1_device.data_size = 0;
  INIT_LIST_HEAD(&(asgn1_device.mem_list));
  asgn1_device.cache = kmem_cache_create("asgn1_cache", sizeof(page_node), 0, 0, NULL);

  //Allocate the device region
  result = alloc_chrdev_region(&(asgn1_device.dev), 0, 1, MYDEV_NAME);
  if(result < 0) return result;

  //Create the cdev and initialize its owner
  asgn1_device.cdev = (struct cdev*)kmalloc(sizeof(struct cdev), GFP_KERNEL);
  cdev_init(asgn1_device.cdev, &asgn1_fops);
  asgn1_device.cdev->owner = THIS_MODULE;
  result = cdev_add(asgn1_device.cdev, asgn1_device.dev, 1);
  if(result){ //Error occurred
	printk(KERN_WARNING "Failed to add device\n");
  }

  //Create the device class
  asgn1_device.class = class_create(THIS_MODULE, MYDEV_NAME);
  if (IS_ERR(asgn1_device.class)) { //Error occurred
 	printk(KERN_WARNING "Failed to create device class");
	result = -ENOMEM;
	goto fail_class;
  }

  //Create the device udev entry
  asgn1_device.device = device_create(asgn1_device.class, NULL, 
                                      asgn1_device.dev, "%s", MYDEV_NAME);
  if (IS_ERR(asgn1_device.device)) { //Error occurred
    printk(KERN_WARNING "%s: can't create udev device\n", MYDEV_NAME);
    result = -ENOMEM;
    goto fail_device;
  }
 
  //Log that initialization was successful
  printk(KERN_WARNING "set up udev entry\n");
  printk(KERN_WARNING "Hello world from %s\n", MYDEV_NAME);
  return 0;

  /* cleanup code called when any of the initialization steps fail */
fail_device:
   class_destroy(asgn1_device.class);

fail_class:
   cdev_del(asgn1_device.cdev);
   unregister_chrdev_region(asgn1_device.dev, 1);

  return result;
}


/**
 * Finalise the module
 */
void __exit asgn1_exit_module(void){

  //Remove the proc entry
  remove_proc_entry("asgn1", NULL);

  //Free all allocated memory pages and destroy the cache
  free_memory_pages();
  kmem_cache_destroy(asgn1_device.cache); 

  //Clean up the udev entry
  device_destroy(asgn1_device.class, asgn1_device.dev);
  class_destroy(asgn1_device.class);
  printk(KERN_WARNING "cleaned up udev entry\n");
  
  //Unregister the cdev
  cdev_del(asgn1_device.cdev);
  unregister_chrdev_region(asgn1_device.dev, 1);
  
  //Log the exit was successful
  printk(KERN_WARNING "Good bye from %s\n", MYDEV_NAME);
}

//Set the init and exit functions
module_init(asgn1_init_module);
module_exit(asgn1_exit_module);


