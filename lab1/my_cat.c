#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>
#include<stdlib.h>
#include<errno.h>
#include<stdio.h>

//Read/Write buffer size
#define BUFFER_SIZE 100

void cleanup(int fd){
	//Close the file
	while(close(fd) != 0){//An error occured
		if(errno == EINTR){ //The close was interrupted
			continue;
		}
		else{ //Some other error
			perror("close()");
			exit(EXIT_FAILURE);
		}
	}
}

//Program entry point
int main(int argc, char** argv)
{
	char fname[100];/*Filename string*/
	int fd; /*File descriptor to read from*/
	char buf[BUFFER_SIZE]; /*Buffer to read into/from*/
	ssize_t size_read, size_written, size_current_write; /*Sizes for the reads and writes*/

	if(argc == 1){//If no options are given use stdin
		scanf("%s", &fname);
	}
	else{//Otherwise take the file name from argv
		strcpy(fname, argv[1]);
	}

	//Open the file if it's a command line option
	while((fd = open(fname, O_RDONLY))<0){//Some error occured opening the file
		if(errno == EINTR){ //Opening the file was interrupted
			continue;
		}
		else{ //Some other error
			perror("open()");
			exit(EXIT_FAILURE);
		}
	}

	//Read until EOF
	while((size_read = read(fd,buf,BUFFER_SIZE)) != 0){

		if(size_read < 0){ //An error occured
			if(errno == EINTR){ //The read was interrupted
				continue;
			}
			else{ //Some other error
				perror("read()");
				cleanup(fd);
				exit(EXIT_FAILURE);
			}
		}
	
		//Write the read buffers contents to stdout
		size_written = 0;
		while((size_current_write = write(STDOUT_FILENO, &buf[size_written], size_read))<size_read-size_written){
			//An error occured while writing
			if(size_current_write < 0){
				if(errno == EINTR){//The write was interrupted
					continue;
				}
				else{//Some other error
					perror("write()");
					cleanup(fd);
					exit(EXIT_FAILURE);
				}
			}
			//Update the amount we have written in total
			size_written += size_current_write;
		}

		
	}

	cleanup(fd);
	exit(EXIT_SUCCESS);
}
