#include<stdlib.h>
#include<stdio.h>
#include<sys/ioctl.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>

int main(){
	int testfd;
	char c[11];
	c[10] = '\0';
	for(int i = 0; i<10; i++){
		c[i] = '0'+i;
	}
	if((testfd = open("/dev/lab6", O_RDONLY))<0){
		printf("Error opening device\n");
	}

	ioctl(testfd, _IO('k',1));
	ioctl(testfd, _IOC(_IOC_READ | _IOC_WRITE,'k',2,10), &c);
	printf("%s\n", c);
	return 0;
}
