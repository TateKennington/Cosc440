#include<linux/module.h>
#include<linux/moduleparam.h>
#include<linux/init.h>
#include<linux/kernel.h>
#include<linux/slab.h>
#include<linux/fs.h>
#include<linux/errno.h>
#include<linux/types.h>
#include<linux/proc_fs.h>
#include<linux/fcntl.h>
#include<linux/aio.h>
#include<asm/uaccess.h>

#include<linux/ioctl.h>
#include<linux/cdev.h>
#include<linux/device.h>

#define MYMAGIC 'k'
#define MYTEST _IO(MYMAGIC, 1)
#define MYVARIABLEREAD _IO(MYMAGIC, 2)

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Tate Kennington");
MODULE_DESCRIPTION("Module for Cosc440 Lab 6");

struct cdev cdev;
struct class *class;
struct device *device; 
int major = 0;

long lab6_ioctl(struct file *filp, unsigned int cmd, unsigned long arg){
	printk(KERN_INFO "ioctl called\n");
	
	if(_IOC_TYPE(cmd) != MYMAGIC) return -EINVAL; 

	if(_IOC_TYPE(cmd) == MYMAGIC && _IOC_NR(cmd) == 2 && _IOC_DIR(cmd) == _IOC_READ | _IOC_WRITE){
		printk(KERN_INFO "Doing read\n");
		char c = 'a';
		char *usrdata = (char*)kmalloc((_IOC_SIZE(cmd)+1)*sizeof(char), GFP_KERNEL);
		usrdata[_IOC_SIZE(cmd)] = '\0';
		copy_from_user(usrdata, (void*)arg, _IOC_SIZE(cmd));
		int i;
		printk(KERN_INFO "Value:%s\n", usrdata);
		printk(KERN_INFO "Size:%d\n", _IOC_SIZE(cmd));
		for(i = 0; i<_IOC_SIZE(cmd); i++){
			copy_to_user((void*)arg, &c, sizeof(c));
			arg++;
			c++;
		}
		return 0;
	}

	switch(cmd){
		case MYTEST:
			printk(KERN_INFO "Testing ioctl\n");
			return 0;
		default: 
			return -EINVAL;
	}
}

struct file_operations lab6_fops = {
	.unlocked_ioctl = lab6_ioctl,
};

int __init lab6_init_module(void){
	int rv;
	dev_t devno = MKDEV(0,0);

	rv = alloc_chrdev_region(&devno, 0, 1, "lab6");

	if(rv < 0) return rv;

	cdev_init(&cdev, &lab6_fops);
	cdev.owner = THIS_MODULE;
	rv = cdev_add(&cdev, devno, 1);
	major = MAJOR(devno);

	if(rv) printk(KERN_WARNING "Error adding device\n");
	else printk(KERN_INFO "Device added successfully\n");

	class = class_create(THIS_MODULE, "lab6");
	if(IS_ERR(class)){
		cdev_del(&cdev);
		unregister_chrdev_region(devno, 1);
		printk(KERN_WARNING "Failed to create class\n");
		rv = -ENOMEM;
		return rv;
	}
	else printk(KERN_INFO "Added class successfully\n");

	device = device_create(class, NULL, devno, "%s", "lab6");

	if(IS_ERR(device)){
		class_destroy(class);
		cdev_del(&cdev);
		unregister_chrdev_region(devno, 1);
		printk(KERN_WARNING "Failed to create udev device\n");
		rv = -ENOMEM;
		return rv;

	}
	else printk(KERN_INFO "Successfully created udev device\n");

	printk(KERN_INFO "Loaded Lab 6 Module\n");
	return 0;
}

void __exit lab6_exit_module(void){
	device_destroy(class, MKDEV(major, 0));
	class_destroy(class);
	cdev_del(&cdev);
	unregister_chrdev_region(MKDEV(major, 0), 1);
	printk(KERN_INFO "Unloaded Lab 6 Module\n");
}

module_init(lab6_init_module);
module_exit(lab6_exit_module);
